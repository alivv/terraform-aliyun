variable "service" {}
variable "environment" {}
variable "subzone" {
  default = "b"
}

variable "region" {
  default = "cn-shanghai"
}

variable "tags" {
  default = {}
}

variable "security_groups" {
  type = list
}

variable "vpc_id" {}
variable "vswitch_id" {
  default = "b"
}

variable "vswitch_info" {
  type = map
}


variable "instance_count" {
  default = 0
}

variable "instance_type" {
  default = "ecs.t5-lc1m1.small"
}

variable "system_disk_size" {
  default = 40
}

variable "system_disk_category" {
  default = "cloud_efficiency"
}

variable "alicloud_availability_zones" {
  default = ["a", "b", "c", "d", "e", "f"]
}

variable "charge_type" {
  default = "PrePaid"
}

variable "charge_period" {
  default = "1"
}

variable "os" {
  default = "ubuntu_16"
}

variable "password" {
  default = "Test@2020"
}

variable "image_id" {
  default = ""
}

