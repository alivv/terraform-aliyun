### #创建VPC, vswitch, EIP,  nat gateway, security_group

```
#网络参考于xxx生产环境简化版, 三个网段demo, prod, k8s, 六个可用区a,b,c,d,e,f
#内部服务实例不绑定公网IP,网络出口使用NAT网关
#　实例 -> vswitch -> nat gateway -> EIP(snat) -> Internet

cd /opt/terraform-aliyun/modules/network

terraform  init
terraform　plan
terraform　apply

#销毁
#terraform　destroy

#注意, 不要反复创建网络资源
＃aliyun nat网关最低按12元/天,其它按小时 
```
