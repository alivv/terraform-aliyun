resource "alicloud_nat_gateway" "cn_shanghai_elvin" {
  vpc_id = alicloud_vpc.cn_shanghai_elvin.id
  specification  = "Small"
  name   = "cn_shanghai_elvin_nat"
#   instance_charge_type = "PostPaid"

  depends_on = [
    alicloud_vswitch.cn_shanghai_elvin_demo_a,
    alicloud_vswitch.cn_shanghai_elvin_demo_b,
    alicloud_vswitch.cn_shanghai_elvin_demo_c,
    alicloud_vswitch.cn_shanghai_elvin_demo_d,
    alicloud_vswitch.cn_shanghai_elvin_demo_e,
    alicloud_vswitch.cn_shanghai_elvin_demo_f,
    alicloud_vswitch.cn_shanghai_elvin_prod_a,
    alicloud_vswitch.cn_shanghai_elvin_prod_b,
    alicloud_vswitch.cn_shanghai_elvin_prod_c,
    alicloud_vswitch.cn_shanghai_elvin_prod_d,
    alicloud_vswitch.cn_shanghai_elvin_prod_e,
    alicloud_vswitch.cn_shanghai_elvin_prod_f,
    alicloud_vswitch.cn_shanghai_elvin_k8s_a,
    alicloud_vswitch.cn_shanghai_elvin_k8s_b,
    alicloud_vswitch.cn_shanghai_elvin_k8s_c,
    alicloud_vswitch.cn_shanghai_elvin_k8s_d,
    alicloud_vswitch.cn_shanghai_elvin_k8s_e,
    alicloud_vswitch.cn_shanghai_elvin_k8s_f,
    alicloud_eip.elvin_nat_eip,
  ]
}

#eip
resource "alicloud_eip_association" "eip_asso" {
  allocation_id = alicloud_eip.elvin_nat_eip.id
  instance_id   = alicloud_nat_gateway.cn_shanghai_elvin.id
}


#demo sw
resource "alicloud_snat_entry" "cn_shanghai_elvin_demo_a" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_demo_a.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_demo_b" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_demo_b.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_demo_c" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_demo_c.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_demo_d" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_demo_d.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_demo_e" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_demo_e.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_demo_f" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_demo_f.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

#prod sw
resource "alicloud_snat_entry" "cn_shanghai_elvin_prod_a" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_prod_a.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_prod_b" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_prod_b.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_prod_c" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_prod_c.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_prod_d" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_prod_d.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_prod_e" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_prod_e.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_prod_f" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_prod_f.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}


#k8s sw
resource "alicloud_snat_entry" "cn_shanghai_elvin_k8s_a" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_k8s_a.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_k8s_b" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_k8s_b.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_k8s_c" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_k8s_c.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_k8s_d" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_k8s_d.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_k8s_e" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_k8s_e.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

resource "alicloud_snat_entry" "cn_shanghai_elvin_k8s_f" {
  snat_table_id     = alicloud_nat_gateway.cn_shanghai_elvin.snat_table_ids
  source_vswitch_id = alicloud_vswitch.cn_shanghai_elvin_k8s_f.id
  snat_ip           = alicloud_eip.elvin_nat_eip.ip_address
}

