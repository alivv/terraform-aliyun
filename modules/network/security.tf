#vlan all
resource "alicloud_security_group" "elvin_all" {
  description   = "allow_all_internal_access"
  name          = "elvin_vlan_all"
  vpc_id        = alicloud_vpc.cn_shanghai_elvin.id
}

resource "alicloud_security_group_rule" "allow_all_internal_access" {
  type              = "ingress"
  ip_protocol       = "all"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "1/65535"
  priority          = 1
  security_group_id = alicloud_security_group.elvin_all.id
  cidr_ip           = var.network
}

#demo
resource "alicloud_security_group" "elvin_demo" {
  name = "elvin_vlan_demo"
  vpc_id = alicloud_vpc.cn_shanghai_elvin.id
}

resource "alicloud_security_group_rule" "tcp_allow_all_demo_access" {
  type              = "ingress"
  ip_protocol       = "all"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "1/65535"
  priority          = 1
  security_group_id = alicloud_security_group.elvin_demo.id
  cidr_ip           = var.subnets["demo"]
}

#prod
resource "alicloud_security_group" "elvin_prod" {
  name = "elvin_vlan_prod"
  vpc_id = alicloud_vpc.cn_shanghai_elvin.id
}

resource "alicloud_security_group_rule" "tcp_allow_all_prod_access" {
  type              = "ingress"
  ip_protocol       = "all"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "1/65535"
  priority          = 1
  security_group_id = alicloud_security_group.elvin_prod.id
  cidr_ip           = var.subnets["prod"]
}

#k8s
resource "alicloud_security_group" "elvin_k8s" {
  name = "elvin_vlan_k8s"
  vpc_id = alicloud_vpc.cn_shanghai_elvin.id
}

resource "alicloud_security_group_rule" "tcp_allow_all_k8s_access" {
  type              = "ingress"
  ip_protocol       = "all"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "1/65535"
  priority          = 1
  security_group_id = alicloud_security_group.elvin_k8s.id
  cidr_ip           = var.subnets["k8s"]
}

#web
resource "alicloud_security_group" "elvin_web" {
  name = "elvin_web"
  vpc_id = alicloud_vpc.cn_shanghai_elvin.id
}

resource "alicloud_security_group_rule" "tcp_allow_all_80_access" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "80/80"
  priority          = 1
  security_group_id = alicloud_security_group.elvin_web.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "tcp_allow_all_443_access" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "443/443"
  priority          = 1
  security_group_id = alicloud_security_group.elvin_web.id
  cidr_ip           = "0.0.0.0/0"
}

#ssh
resource "alicloud_security_group" "elvin_ssh" {
  name = "elvin_ssh"
  vpc_id = alicloud_vpc.cn_shanghai_elvin.id
}

resource "alicloud_security_group_rule" "tcp_allow_ssh_access" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "22/22"
  priority          = 1
  security_group_id = alicloud_security_group.elvin_ssh.id
  cidr_ip           = "0.0.0.0/0"
}
