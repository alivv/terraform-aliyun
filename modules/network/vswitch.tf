#demo
resource "alicloud_vswitch" "cn_shanghai_elvin_demo_a" {
  name              = "elvin_demo_a"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_demo_subnets["a"]
  availability_zone = "cn-shanghai-a"
  tags              = map("env", "demo")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_demo_b" {
  name              = "cn_shanghai_elvin_demo_b"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_demo_subnets["b"]
  availability_zone = "cn-shanghai-b"
  tags              = map("env", "demo")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_demo_c" {
  name              = "cn_shanghai_elvin_demo_c"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_demo_subnets["c"]
  availability_zone = "cn-shanghai-c"
  tags              = map("env", "demo")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_demo_d" {
  name              = "cn_shanghai_elvin_demo_d"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_demo_subnets["d"]
  availability_zone = "cn-shanghai-d"
  tags              = map("env", "demo")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_demo_e" {
  name              = "cn_shanghai_elvin_demo_e"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_demo_subnets["e"]
  availability_zone = "cn-shanghai-e"
  tags              = map("env", "demo")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_demo_f" {
  name              = "cn_shanghai_elvin_demo_f"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_demo_subnets["f"]
  availability_zone = "cn-shanghai-f"
  tags              = map("env", "demo")
}


#prod
resource "alicloud_vswitch" "cn_shanghai_elvin_prod_a" {
  name              = "cn_shanghai_elvin_prod_a"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_prod_subnets["a"]
  availability_zone = "cn-shanghai-a"
  tags              = map("env", "prod")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_prod_b" {
  name              = "cn_shanghai_elvin_prod_b"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_prod_subnets["b"]
  availability_zone = "cn-shanghai-b"
  tags              = map("env", "prod")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_prod_c" {
  name              = "cn_shanghai_elvin_prod_c"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_prod_subnets["c"]
  availability_zone = "cn-shanghai-c"
  tags              = map("env", "prod")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_prod_d" {
  name              = "cn_shanghai_elvin_prod_d"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_prod_subnets["d"]
  availability_zone = "cn-shanghai-d"
  tags              = map("env", "prod")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_prod_e" {
  name              = "cn_shanghai_elvin_prod_e"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_prod_subnets["e"]
  availability_zone = "cn-shanghai-e"
  tags              = map("env", "prod")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_prod_f" {
  name              = "cn_shanghai_elvin_prod_f"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_prod_subnets["f"]
  availability_zone = "cn-shanghai-f"
  tags              = map("env", "prod")
}

#k8s
resource "alicloud_vswitch" "cn_shanghai_elvin_k8s_a" {
  name              = "cn_shanghai_elvin_k8s_a"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_k8s_subnets["a"]
  availability_zone = "cn-shanghai-a"
  tags              = map("env", "k8s")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_k8s_b" {
  name              = "cn_shanghai_elvin_k8s_b"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_k8s_subnets["b"]
  availability_zone = "cn-shanghai-b"
  tags              = map("env", "k8s")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_k8s_c" {
  name              = "cn_shanghai_elvin_k8s_c"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_k8s_subnets["c"]
  availability_zone = "cn-shanghai-c"
  tags              = map("env", "k8s")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_k8s_d" {
  name              = "cn_shanghai_elvin_k8s_d"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_k8s_subnets["d"]
  availability_zone = "cn-shanghai-d"
  tags              = map("env", "k8s")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_k8s_e" {
  name              = "cn_shanghai_elvin_k8s_e"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_k8s_subnets["e"]
  availability_zone = "cn-shanghai-e"
  tags              = map("env", "k8s")
}

resource "alicloud_vswitch" "cn_shanghai_elvin_k8s_f" {
  name              = "cn_shanghai_elvin_k8s_f"
  vpc_id            = alicloud_vpc.cn_shanghai_elvin.id
  cidr_block        = var.elvin_k8s_subnets["f"]
  availability_zone = "cn-shanghai-f"
  tags              = map("env", "k8s")
}
