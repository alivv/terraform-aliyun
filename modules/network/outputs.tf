
output "vpc" {
  value = "${map(
    "id", alicloud_vpc.cn_shanghai_elvin.id,
    "cidr", alicloud_vpc.cn_shanghai_elvin.cidr_block
  )}"
}

output "vswitch_info" {
  value = "${
    map(
      "demo", "${map(
        "a", "${alicloud_vswitch.cn_shanghai_elvin_demo_a.id}",
        "b", "${alicloud_vswitch.cn_shanghai_elvin_demo_b.id}",
        "c", "${alicloud_vswitch.cn_shanghai_elvin_demo_c.id}",
        "d", "${alicloud_vswitch.cn_shanghai_elvin_demo_d.id}",
        "e", "${alicloud_vswitch.cn_shanghai_elvin_demo_e.id}",
        "f", "${alicloud_vswitch.cn_shanghai_elvin_demo_f.id}"
      )}",
      "prod", "${map(
        "a", "${alicloud_vswitch.cn_shanghai_elvin_prod_a.id}",
        "b", "${alicloud_vswitch.cn_shanghai_elvin_prod_b.id}",
        "c", "${alicloud_vswitch.cn_shanghai_elvin_prod_c.id}",
        "d", "${alicloud_vswitch.cn_shanghai_elvin_prod_d.id}",
        "e", "${alicloud_vswitch.cn_shanghai_elvin_prod_e.id}",
        "f", "${alicloud_vswitch.cn_shanghai_elvin_prod_f.id}"
      )}",
      "k8s", "${map(
        "a", "${alicloud_vswitch.cn_shanghai_elvin_k8s_a.id}",
        "b", "${alicloud_vswitch.cn_shanghai_elvin_k8s_b.id}",
        "c", "${alicloud_vswitch.cn_shanghai_elvin_k8s_c.id}",
        "d", "${alicloud_vswitch.cn_shanghai_elvin_k8s_d.id}",
        "e", "${alicloud_vswitch.cn_shanghai_elvin_k8s_e.id}",
        "f", "${alicloud_vswitch.cn_shanghai_elvin_k8s_f.id}"
      )}"
    )
  }"
}

output "security_group" {
  value = "${map(
    "all",   alicloud_security_group.elvin_all.id,
    "demo",  alicloud_security_group.elvin_demo.id,
    "prod",  alicloud_security_group.elvin_prod.id,
    "k8s",   alicloud_security_group.elvin_k8s.id,
    "web",   alicloud_security_group.elvin_web.id,
    "ssh",   alicloud_security_group.elvin_ssh.id
  )}"
}


output "elvin_nat_eip" {
  value = "${map(
    "id", alicloud_eip.elvin_nat_eip.id,
    "ip", alicloud_eip.elvin_nat_eip.ip_address
  )}"
}

output "eip_elvin_bastion" {
  value = "${map(
    "id",  alicloud_eip.elvin_bastion.id,
    "ip",  alicloud_eip.elvin_bastion.ip_address
  )}"
}
