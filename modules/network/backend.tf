
terraform {
  backend "oss" {
    bucket              = "terraform-elvin"
    prefix              = "terraform"
    key                 = "network/terraform.tfstate"
    acl                 = "private"
    encrypt             = "true"

    region              = "cn-shanghai"
    profile             = "default"
  }
}


#
# https://developer.aliyun.com/article/723316
# https://www.terraform.io/docs/backends/types/oss.html
