### #创建OSS

```
#初始化
terraform init

#执行计划
terraform　plan

#创建资源(等待输入yes)
terraform　apply 

#使用oss存储Terraform Backend状态文件(不配置默认本地) 
```
>  参考  
>  https://registry.terraform.io/providers/aliyun/alicloud/latest/docs/resources/oss_bucket

