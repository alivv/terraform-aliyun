
# provider "alicloud" {
# }

# test

# 资源名称
variable "name" {
  default = "elvin-k8s-demo"
}
# 日志服务名称
variable "log_project_name" {
  default = "elvin-k8s-log-demo"
}
# # 可用区
# data "alicloud_zones" default {
#   available_resource_creation = "VSwitch"
# }
variable "alicloud_availability_zones" {
  default = ["a", "b", "c"]
}

# 节点ECS实例配置
data "alicloud_instance_types" "default" {
  # availability_zone    = data.alicloud_zones.default.zones[0].id
  cpu_core_count       = 2
  memory_size          = 4
  kubernetes_node_role = "Worker"
}
# # 专有网络
# resource "alicloud_vpc" "default" {
#   name       = var.name
#   cidr_block = "10.1.0.0/21"
# }
# # 交换机
# resource "alicloud_vswitch" "default" {
#   name              = var.name
#   vpc_id            = alicloud_vpc.default.id
#   cidr_block        = "10.1.1.0/24"
#   availability_zone = data.alicloud_zones.default.zones[0].id
# }

# 日志服务
resource "alicloud_log_project" "log" {
  name        = var.log_project_name
  description = "created by terraform for managedkubernetes cluster"
}

locals {
  k8s_vswitch = data.terraform_remote_state.network.outputs.vswitch_info["k8s"]
}

# variable "count" {
#   default = 2
# }

# kubernetes托管版
resource "alicloud_cs_managed_kubernetes" "default" {
  # kubernetes集群名称
  name                      = var.name
  # 新的kubernetes集群将位于的区域
  availability_zone         = "var.alicloud_availability_zones"
  # 新的kubernetes集群将位于的vswitch。指定一个或多个vswitch的ID。它必须在availability_zone指定的区域中
  # vswitch_ids               = data.terraform_remote_state.network.outputs.vswitch_info["k8s"]
  # worker_vswitch_ids        = data.terraform_remote_state.network.outputs.vswitch_info["k8s"]
  # worker_vswitch_ids        = local.k8s_vswitch["${element(var.alicloud_availability_zones, 2)}"]
  worker_vswitch_ids        = ["vsw-uf6k1tek9c6cezfe28khc", "vsw-uf6okmdnsqtzic05911j3", "vsw-uf6e0iiifnut7mg8fz9h8"]
#   worker_vswitch_ids        = ["local.k8s_vswitch.a", "local.k8s_vswitch.b", "local.k8s_vswitch.c"]
  # worker_vswitch_ids        = ["vsw-uf6k1tek9c6cezfe28khc"]
  # count = 2
  # worker_vswitch_ids        = ["{local.k8s_vswitch["${element(var.alicloud_availability_zones,  count.index)}"]}"]
  # 是否在创建kubernetes集群时创建新的nat网关。默认为true。
  new_nat_gateway           = false
  # nat_gateway_id            = data.terraform_remote_state.network.outputs.elvin_nat_eip.id
  # vpc_id                    = "data.terraform_remote_state.network.outputs.vpc.id"
  # 节点的ECS实例类型
  worker_instance_types     = [data.alicloud_instance_types.default.instance_types[0].id]
  # kubernetes群集的总工作节点数。默认值为3。最大限制为50
  worker_number             = 2
  # ssh登录群集节点的密码,必须指定password或key_name
  password                  = "K8s@2020"
  # pod网络的CIDR块。当cluster_network_type设置为flannel，必须设定该参数
  pod_cidr                  = "172.20.0.0/16"
  # 服务网络的CIDR块。它不能与VPC CIDR相同，不能与VPC中的Kubernetes群集使用的CIDR相同，也不能在创建后进行修改。
  service_cidr              = "172.21.0.0/20"
  # 是否为kubernetes的节点安装云监控。
  install_cloud_monitor     = true
  # 是否为API Server创建Internet负载均衡。默认为false。
  slb_internet_enabled      = false
  # 节点的系统磁盘类别。其有效值为cloud_ssd和cloud_efficiency。默认为cloud_efficiency。
  worker_disk_category      = "cloud_efficiency"
  # 节点的数据磁盘类别。其有效值为cloud_ssd和cloud_efficiency，如果未设置，将不会创建数据磁盘。
  # worker_data_disk_category = "cloud_efficiency"
  # # 节点的数据磁盘大小。有效值范围[20〜32768]，以GB为单位。当worker_data_disk_category被呈现，则默认为40。
  # worker_data_disk_size     = 40
  #系统CentOS7或AliyunLinux2
  # image_id                  = "AliyunLinux2"
  # security_group_id         = ["data.terraform_remote_state.network.outputs.security_group.all","data.terraform_remote_state.network.outputs.security_group.ssh"]
  security_group_id         = "sg-uf696t897o8r5jtemnmo"
  user_data                 = file("${path.module}/setup.sh")

#   # 日志配置
#   log_config {
#     # 收集日志的类型，目前仅支持SLS。
#     type    = "SLS"
#     # 日志服务项目名称，群集日志将输出到该项目
#     project = alicloud_log_project.log.name
#   }
} 

