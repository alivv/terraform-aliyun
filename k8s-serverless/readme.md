
### #创建Aliyun Serverless Kubernetes

```
#Serverless集群不创建k8s master和node节点，按POD资源收费

terraform  init
terraform　plan
terraform　apply

#创建完成后，查看web控制台.kube/config配置 
```

>  参考  
>  https://registry.terraform.io/providers/aliyun/alicloud/latest/docs/resources/cs_serverless_kubernetes
