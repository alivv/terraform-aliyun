

Terraform Aliyun 创建ecs, kubernetes 实例
==========

> terraform demo  for aliyun  
> 创建vpc, 网关, EIP, ecs, kubernetes, Serverless Kubernetes  
> env :   
> Terraform v0.12.28   
> provider.alicloud v1.92.0  
>
> By Elvin ,2020-8-23, <a href="http://blog.elvin.vip " target="_blank">http://blog.elvin.vip </a>  
> git source <a href="https://gitee.com/alivv/terraform-aliyun.git" target="_blank">https://gitee.com/alivv/terraform-aliyun.git</a>  

---

### #环境准备  

##### #aliyun cli

```
#下载和安装 download and install
wget https://aliyuncli.alicdn.com/aliyun-cli-linux-latest-amd64.tgz
tar -zxf aliyun-cli-linux-latest-amd64.tgz -C /usr/local/bin/

#配置认证 config AccessKey,SecretKey,Region
aliyun configure --profile default

#list config 列出配置
aliyun configure list --profile default

# --profile 配置名,用于管理多配置,默认名称default
```

> 阿里云帮助文档 aliyun help doc  
>  https://help.aliyun.com/document_detail/121541.html  
> https://help.aliyun.com/document_detail/121198.html  

##### #terraform

```
#download and install
wget http://files.elvin.vip/cli/terraform_0.12.28_linux_amd64.zip
unzip -oq terraform_0.12.28_linux_amd64.zip -d /usr/local/bin/
```
##### #provider-alicloud
```
wget http://files.elvin.vip/cli/terraform-provider-alicloud_1.92.0_linux_amd64.zip
mkdir -p ~/.terraform.d/plugins
unzip -oq terraform-provider-alicloud_1.92.0_linux_amd64.zip -d ~/.terraform.d/plugins/

#provider资源下载保存到terraform相同目录,或~/.terraform.d/plugins/
#避免terraform init初始化时每个目录自动下载provider而耗时 !!!
```

> 官网 download url  
> https://releases.hashicorp.com/terraform/  
> https://releases.hashicorp.com/terraform-provider-alicloud/  

##### #clone demo source  
```
git clone https://gitee.com/alivv/terraform-aliyun.git /opt/
```

---

### #创建OSS

```
cd /opt/terraform-aliyun/modules/oss/

#初始化
terraform init

#执行计划
terraform　plan

#创建资源(等待输入yes)
terraform　apply 
```

##### #创建oss资源 实例

```
resource "alicloud_oss_bucket" "terraform-elvin" { 
    bucket = "terraform-elvin" 
    #bucket名称已存在会创建失败
    acl = "private"
    tags = {
        "Terraform" = "true"
    }
    versioning {
        status = "Enabled"
    }
}

#使用oss存储Terraform Backend状态文件(不配置默认本地) 
＃后面实例需要修改backend.tf文件bucket对应名称 
```

---

### #创建VPC, vswitch, EIP,  nat gateway, security_group

```
#网络参考于xxx生产环境简化版, 三个网段demo, prod, k8s, 六个可用区a,b,c,d,e,f
#内部服务实例不绑定公网IP,网络出口使用NAT网关
#　实例 -> vswitch -> nat gateway -> EIP(snat) -> Internet

cd /opt/terraform-aliyun/modules/network

terraform  init
terraform　plan
terraform　apply

#销毁
#terraform　destroy

#注意, 不要反复创建网络资源
＃aliyun nat网关最低按12元/天,其它按小时 
```

### #创建一个ECS实例作为跳板机

```
#使用已有网络创建bastion(VM),并绑定EIP,作为跳板机

cd /opt/terraform-aliyun/bastion
terraform  init

#根据workspace使用demo网段
terraform workspace new demo
terraform workspace select demo
terraform workspace list

terraform　apply
```

---

### #创建Aliyun Serverless Kubernetes

```
#Serverless集群不创建k8s master和node节点，按POD资源收费，和适合测试或运行小应用
cd /opt/terraform-aliyun/k8s-serverless
terraform  init
terraform　plan
terraform　apply
＃创建完成后，查看web控制台.kube/config配置，在bastion使用(没配置公网IP, 使用内网IP访问)
```

#### #创建Aliyun Kubernetes 托管版

```
#aliyun Kubernetes 托管版,只有work节点, master节点使用托管(共享资源，资源隔离)
cd /opt/terraform-aliyun/k8s-node
#根据需要更改实例规格和节点数量
terraform  init
terraform　apply
```

#### #创建Aliyun Kubernetes 标准版

```
#配置实例为３个master节点, 2个work节点
cd /opt/terraform-aliyun/kubernetes-cluster
terraform  init
terraform　plan
#master节点创建成功, work节点失败，多次创建失败　...
```

---

> 更多详情，查看git仓库 <a href="https://gitee.com/alivv/terraform-aliyun.git" target="_blank">https://gitee.com/alivv/terraform-aliyun.git</a>  

##### #创建pvc的outpouts  
![](http://imgs.elvin.vip/2020/elvin.vpc.png)
