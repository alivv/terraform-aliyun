

resource "alicloud_cs_kubernetes" "k8s" {
  name                  = "k8s-cluster-demo"
  # count                 = 1
  master_vswitch_ids    = ["${local.k8s_vswitch.a}", "${local.k8s_vswitch.b}", "${local.k8s_vswitch.c}"]
  worker_vswitch_ids    = ["${local.k8s_vswitch.a}", "${local.k8s_vswitch.b}", "${local.k8s_vswitch.c}"]
  master_instance_types = var.master_instance_types
  worker_instance_types = var.worker_instance_types
  security_group_id     = local.security_group.all
  worker_number         = 2
  enable_ssh            = true
  install_cloud_monitor = true
  proxy_mode            = "ipvs"
  password              = var.password
  pod_cidr              = "172.20.0.0/16"
  service_cidr          = "172.21.0.0/20"
  slb_internet_enabled  = false
  new_nat_gateway       = false
  worker_disk_category  = "cloud_efficiency"
  master_disk_category  = "cloud_efficiency"
  master_disk_size      = 40
  worker_disk_size      = 40
  # cluster_network_type  =  "flannel"
  # version               = "1.16.9-aliyun.1
  user_data                 = file("${path.module}/setup.sh")
  # dynamic "addons" {
  #     for_each = var.cluster_addons
  #     content {
  #       name                    = lookup(addons.value, "name", var.cluster_addons)
  #       config                  = lookup(addons.value, "config", var.cluster_addons)
  #     }
  # }
}

#参考
#https://registry.terraform.io/providers/aliyun/alicloud/latest/docs/resources/cs_kubernetes
