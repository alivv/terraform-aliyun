
// Output kubernetes resource
output "name" {
  value       = alicloud_cs_kubernetes.k8s.*.name
}

output "version" {
  value       = alicloud_cs_kubernetes.k8s.*.version
}

output "cluster_id" {
  description = "ID of the kunernetes cluster."
  value       = [alicloud_cs_kubernetes.k8s.*.id]
}

output "worker_nodes" {
  description = "List worker nodes of cluster."
  value       = [alicloud_cs_kubernetes.k8s.*.worker_nodes]
}

output "master_nodes" {
  description = "List master nodes of cluster."
  value       = [alicloud_cs_kubernetes.k8s.*.master_nodes]
}

output "connections" {
  value       = alicloud_cs_kubernetes.k8s.*.connections
}
output "worker_ram_role_name" {
  value       = alicloud_cs_kubernetes.k8s.*.worker_ram_role_name
}

